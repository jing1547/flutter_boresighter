import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_libserialport/flutter_libserialport.dart';
import 'package:flutter_spinbox/flutter_spinbox.dart';

void main() {
  runApp(const App());
}

class App extends StatefulWidget {
  const App({super.key});

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  SerialPort? _serialPort;
  List<SerialPort> portList = [];
  bool isRF = false;
  double val = 0;
  double level = 0;
  int up = 1;
  int numberValue = 0;
  bool showError1 = false;
  bool showError2 = false;
  TextEditingController numberController1 = TextEditingController();
  TextEditingController numberController2 = TextEditingController();
  List<String> comp = [];
  @override
  void initState() {
    super.initState();
    var i = 0;
    for (final name in SerialPort.availablePorts) {
      final sp = SerialPort(name);
      if (kDebugMode) {
        print('${++i}) $name');
        print('\tDescription: ${sp.description ?? ''}');
        print('\tManufacturer: ${sp.manufacturer}');
        print('\tSerial Number: ${sp.serialNumber}');
        print('\tProduct ID: 0x${sp.productId?.toRadixString(16) ?? 00}');
        print('\tVendor ID: 0x${sp.vendorId?.toRadixString(16) ?? 00}');
      }
      portList.add(sp);
    }
  }

  void warning1(value) {
    int parsedValue = value.toInt() ?? 0;

    setState(() {
      bool isInRange1 = parsedValue >= 2200 && parsedValue <= 2300;
      bool isInRange2 = parsedValue >= 8000 && parsedValue <= 8400;
      showError1 = !isInRange1 && !isInRange2;
    });
  }

  void warning2(value) {
    //int parsedValue = value.toInt() ?? 0;

    setState(() {
      //bool isInRange1 = parsedValue >= 2200 && parsedValue <= 2300;
      //bool isInRange2 = parsedValue >= 8000 && parsedValue <= 8400;
      //showError2 = !isInRange1 && !isInRange2;
    });
  }

  @override
  Widget build(BuildContext context) {
    SerialPort port1 = SerialPort('COM1');

    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        body: Row(
          children: [
            Expanded(
              flex: 8,
              child: Column(
                children: [
                  const Expanded(
                    flex: 1,
                    child: SizedBox(
                      height: 150,
                    ),
                  ),
                  Expanded(
                    flex: 4,
                    child: Row(
                      children: [
                        const SizedBox(
                          width: 90,
                        ),
                        const Expanded(
                          flex: 2,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 60,
                              ),
                              Text(
                                '주파수',
                                style: TextStyle(
                                  fontSize: 50,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                              SizedBox(
                                height: 70,
                              ),
                              Text(
                                '신호레벨',
                                style: TextStyle(
                                  fontSize: 50,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Expanded(
                          flex: 5,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Row(
                                children: [
                                  SizedBox(
                                    width: 290,
                                    child: Column(
                                      children: [
                                        SpinBox(
                                          enableInteractiveSelection: false,
                                          value: val,
                                          max: 9999,
                                          step: up.toDouble(),
                                          keyboardType: TextInputType.number,
                                          decoration: InputDecoration(
                                            errorText: showError1
                                                ? 'Warning: 2200~2300 MHz or 8000~8400 MHz'
                                                : null,
                                            labelText: '주파수',
                                          ),
                                          textStyle:
                                              const TextStyle(fontSize: 30),
                                          textAlign: TextAlign.center,
                                          onChanged: (value) {
                                            warning1(value);
                                            val = value;
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 40,
                                  ),
                                  const Text(
                                    'MHz',
                                    style: TextStyle(
                                        fontSize: 50,
                                        fontWeight: FontWeight.w400),
                                  ),
                                  const SizedBox(
                                    width: 50,
                                  ),
                                  Column(
                                    children: [
                                      Container(
                                        clipBehavior: Clip.hardEdge,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(30)),
                                        height: 50,
                                        width: 100,
                                        child: ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              backgroundColor:
                                                  Colors.blue.shade100,
                                            ),
                                            onPressed: () {
                                              setState(() {
                                                val = 2200;

                                                bool isInRange1 =
                                                    val >= 2200 && val <= 2300;
                                                bool isInRange2 =
                                                    val >= 8000 && val <= 8400;
                                                showError1 =
                                                    !isInRange1 && !isInRange2;
                                              });
                                            },
                                            child: const Text(
                                              '2200 MHz',
                                              style: TextStyle(
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.w400,
                                                  color: Colors.black),
                                            )),
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Container(
                                        clipBehavior: Clip.hardEdge,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(30)),
                                        height: 50,
                                        width: 100,
                                        child: ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              backgroundColor:
                                                  Colors.blue.shade100,
                                            ),
                                            onPressed: () {
                                              val = 8000;

                                              showError1 = false;
                                              setState(() {});
                                            },
                                            child: const Text(
                                              '8000 MHz',
                                              style: TextStyle(
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.w400,
                                                  color: Colors.black),
                                            )),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    width: 50,
                                  ),
                                  Column(
                                    children: [
                                      const Text('Offset:'),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Container(
                                        clipBehavior: Clip.hardEdge,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        height: 50,
                                        width: 120,
                                        child: ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              backgroundColor:
                                                  Colors.blue.shade100,
                                            ),
                                            onPressed: () {
                                              setState(() {
                                                up = 1;
                                              });
                                            },
                                            child: const Text(
                                              '1 MHz',
                                              style: TextStyle(
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.w400,
                                                  color: Colors.black),
                                            )),
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Container(
                                        clipBehavior: Clip.hardEdge,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        height: 50,
                                        width: 120,
                                        child: ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              backgroundColor:
                                                  Colors.blue.shade100,
                                            ),
                                            onPressed: () {
                                              setState(() {
                                                up = 10;
                                              });
                                            },
                                            child: const Text(
                                              '10 MHz',
                                              style: TextStyle(
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.w400,
                                                  color: Colors.black),
                                            )),
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Container(
                                        clipBehavior: Clip.hardEdge,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        height: 50,
                                        width: 120,
                                        child: ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              backgroundColor:
                                                  Colors.blue.shade100,
                                            ),
                                            onPressed: () {
                                              setState(() {
                                                up = 100;
                                              });
                                            },
                                            child: const Text(
                                              '100 MHz',
                                              style: TextStyle(
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.w400,
                                                  color: Colors.black),
                                            )),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  SizedBox(
                                    width: 290,
                                    child: Column(
                                      children: [
                                        SpinBox(
                                          max: 24,
                                          min: 0,
                                          value: level,
                                          keyboardType: TextInputType.number,
                                          decoration: InputDecoration(
                                            errorText:
                                                showError2 ? 'Warning' : null,
                                            labelText: '신호레벨',
                                          ),
                                          textStyle:
                                              const TextStyle(fontSize: 30),
                                          textAlign: TextAlign.center,
                                          onChanged: (value) {
                                            level = value;
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 40,
                                  ),
                                  const Text(
                                    'dBm',
                                    style: TextStyle(
                                        fontSize: 50,
                                        fontWeight: FontWeight.w400),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Expanded(
                    flex: 2,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.all(8),
                            color: Colors.grey.shade300,
                            child: ListView.builder(
                              padding: const EdgeInsets.all(8),
                              itemCount: comp.length,
                              itemBuilder: ((context, index) {
                                return Row(
                                  children: [
                                    const SizedBox(
                                      width: 20,
                                    ),
                                    Text(
                                      comp[index],
                                      style: const TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                );
                              }),
                            ),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30),
                          ),
                          clipBehavior: Clip.hardEdge,
                          width: 250,
                          height: 150,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.blue.shade100),
                            onPressed: () {
                              debugPrint("click");
                              setState(() {
                                port1.write(Uint8List.fromList(utf8.encode(
                                    "${val.toInt()}, ${level.toInt()} \r")));
                                comp.add("${val.toInt()}, ${level.toInt()}");
                              });
                            },
                            child: const Text(
                              'SET',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 30,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 80,
                        )
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Expanded(
                    flex: 1,
                    child: Row(
                      children: [
                        const SizedBox(
                          width: 80,
                        ),
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                            ),
                            clipBehavior: Clip.hardEdge,
                            width: 200,
                            height: 110,
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.blue.shade100),
                              onPressed: () {},
                              child: const Text(
                                '장비 Status',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 30,
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                            ),
                            clipBehavior: Clip.hardEdge,
                            width: 200,
                            height: 110,
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.blue.shade100),
                              onPressed: () {},
                              child: const Text(
                                'Lock Status',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 30,
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                            ),
                            clipBehavior: Clip.hardEdge,
                            width: 200,
                            height: 110,
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.blue.shade100),
                              onPressed: () {},
                              child: const Text(
                                '송신 Status',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 30,
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 150,
                        ),
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                            ),
                            clipBehavior: Clip.hardEdge,
                            width: 200,
                            height: 110,
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: isRF
                                      ? Colors.blue.shade400
                                      : Colors.red.shade400),
                              onPressed: () {
                                isRF = !isRF;
                                if (_serialPort == null) {
                                  return;
                                }
                                if (_serialPort!.isOpen) {
                                  _serialPort!.close();
                                  debugPrint('${_serialPort!.name} closed!');
                                } else {
                                  if (_serialPort!
                                      .open(mode: SerialPortMode.readWrite)) {
                                    SerialPortConfig config =
                                        _serialPort!.config;
                                    // https://www.sigrok.org/api/libserialport/0.1.1/a00007.html#gab14927cf0efee73b59d04a572b688fa0
                                    // https://www.sigrok.org/api/libserialport/0.1.1/a00004_source.html
                                    config.baudRate = 19200;
                                    config.parity = 0;
                                    config.bits = 8;
                                    config.cts = 0;
                                    config.rts = 0;
                                    config.stopBits = 1;
                                    config.xonXoff = 0;
                                    _serialPort!.config = config;
                                    if (_serialPort!.isOpen) {
                                      debugPrint(
                                          '${_serialPort!.name} opened!');
                                    }
                                  }
                                }
                                setState(() {});
                              },
                              child: const Text(
                                'RF On/Off',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 30,
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 80,
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
